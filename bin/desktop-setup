#!/bin/bash
set -e

# Not included:
# - browser extensions: metamask & pocket
# - mounting internal hard drive

########################################
# Basic dir setup

mkdir -p "$HOME/bin"

########################################
# Install simple, essential utils

for util in adb cryptsetup curl exiftool ffmpeg git jq make nmap onboard pdftk rename shellcheck sqlite3 tree vim whois xclip
do
  if [[ -z "$(command -v $util)" ]]
  then
    echo
    echo "Installing $util"
    sudo apt-get install -y "$util"
  else echo "$util is already installed"
  fi
done

# Tools whose executable doesn't match the package name
for util in build-essential ca-certificates libopenjp2-tools libudev-dev libusb-1.0-0-dev snapd
do
  if ! dpkg -s "$util" > /dev/null 2>&1
  then
    echo
    echo "Installing $util"
    sudo apt-get install -y "$util" 
  else echo "$util is already installed"
  fi
done

########################################
# Install stuff that requires custom steps

# KeePassXC
if [[ -z "$(command -v keepassxc)" ]]
then
  echo
  echo "Installing KeePassXC"
  sudo add-apt-repository -y ppa:phoerious/keepassxc
  sudo apt update
  sudo apt install -y keepassxc
else 
  echo "KeepassXC is already installed"
fi

# Dropbox
if [[ -z "$(command -v dropbox)" ]]
then
  echo
  echo "Installing Dropbox"
  echo "deb [arch=i386,amd64] http://linux.dropbox.com/ubuntu bionic main" | sudo tee /etc/apt/sources.list.d/dropbox.list
  sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 1C61A2656FB57B7E4DE0F4C1FC918B335044912E
  sudo apt update
  sudo apt install -y python3-gpg dropbox
else 
  echo "Dropbox is already installed"
fi

# Docker
if [[ -z "$(command -v docker)" ]]
then
  echo
  echo "Installing Docker"
  echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo apt update
  sudo apt-get install -y docker-ce
  # post-installation setup
  sudo usermod -aG docker "$(whoami)"
  sudo systemctl enable docker
else 
  echo "Docker is already installed"
  # Restart required before docker permissions are applied
  docker swarm init "--advertise-addr=127.0.0.1" 2> /dev/null || true
fi

# Brave Browser
if [[ -z "$(command -v brave-browser)" ]]
then
  echo
  echo "Installing Brave Browser"
  sudo apt install -y apt-transport-https curl gnupg
  curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
  echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser.list
  sudo apt update
  sudo apt install -y brave-browser
else 
  echo "Brave Browser is already installed"
fi

# Javascript Tools: nvm & npm & node js
export NVM_DIR="$HOME/.nvm";
# shellcheck disable=SC1090
[ -f "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"
if [[ -z "$(command -v npm)" ]]
then
  echo
  echo "Installing nvm & npm & node js"
  # Install NVM first
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/4b947ec92d0195756709e5b563569cf48aef1e09/install.sh | sudo -E bash -
  export NVM_DIR="$HOME/.nvm"
  # shellcheck disable=SC1090
  [ -f "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"
  sudo chown -R "$(id -u):$(id -g)" "$NVM_DIR" "$HOME/.config"
  nvm install 14
  nvm use 14
  node -v
else 
  echo "nvm & npm & node are already installed"
fi

# Javascript CLI Tools
for npmPkg in ts-node surya
do
  if [[ -z "$(command -v $npmPkg)" ]]
  then
    echo
    echo "Installing $npmPkg"
    npm install -g "$npmPkg"
  else echo "$npmPkg is already installed"
  fi
done

# GIMP 
if [[ -z "$(command -v gimp)" ]]
then
  echo
  echo "Installing GIMP"
  sudo apt -y update
  sudo apt install -y gimp
else 
  echo "GIMP is already installed"
fi

# Neovim
if [[ -z "$(command -v nvim)" ]]
then
  echo
  echo "Installing Neovim"
  sudo add-apt-repository -y ppa:neovim-ppa/unstable
  sudo apt-get update -y
  sudo apt-get install -y neovim
  vimdir="$HOME/.config/nvim"
  mkdir -p ~/.vimtmp # This folder will store our temporary backup files
  mkdir -p "$vimdir/autoload" "$vimdir/bundle" "$vimdir/undo"
  # https://github.com/tpope/vim-pathogen
  curl -LSso "$vimdir/autoload/pathogen.vim" https://tpo.pe/pathogen.vim
  (
    cd "$vimdir/bundle"
    [[ -d nerdtree ]] || git clone -q https://github.com/scrooloose/nerdtree.git
    [[ -d supertab ]] || git clone -q https://github.com/ervandew/supertab
    [[ -d syntastic ]] || git clone -q --depth=1 https://github.com/vim-syntastic/syntastic.git
    [[ -d typescript-vim ]] || git clone -q https://github.com/leafgarland/typescript-vim.git
    [[ -d vim-javascript ]] || git clone -q https://github.com/pangloss/vim-javascript.git
    [[ -d vim-jsx ]] || git clone -q https://github.com/mxw/vim-jsx.git
    [[ -d vim-solidity ]] || git clone -q https://github.com/tomlion/vim-solidity.git
    [[ -d vim-vinegar ]] || git clone -q https://github.com/tpope/vim-vinegar.git
  )
  chown -R "$(id -u):$(id -g)" "$vimdir"
else 
  echo "Neovim is already installed"
fi

# Ledger Live
if [[ -z "$(command -v ledger)" ]]
then
  echo
  echo "Installing Ledger Live"

  # Instructions: https://support.ledger.com/hc/en-us/articles/115005165269-Fix-USB-connection-issues-with-Ledger-Live
  # Script from: https://raw.githubusercontent.com/LedgerHQ/udev-rules/master/add_udev_rules.sh
  cat <<-EOF | sudo tee /etc/udev/rules.d/20-hw1.rules
	SUBSYSTEMS=="usb", ATTRS{idVendor}=="2581", ATTRS{idProduct}=="1b7c|2b7c|3b7c|4b7c", TAG+="uaccess", TAG+="udev-acl"
	SUBSYSTEMS=="usb", ATTRS{idVendor}=="2c97", ATTRS{idProduct}=="0000|0000|0001|0002|0003|0004|0005|0006|0007|0008|0009|000a|000b|000c|000d|000e|000f|0010|0011|0012|0013|0014|0015|0016|0017|0018|0019|001a|001b|001c|001d|001e|001f", TAG+="uaccess", TAG+="udev-acl"
	SUBSYSTEMS=="usb", ATTRS{idVendor}=="2c97", ATTRS{idProduct}=="0001|1000|1001|1002|1003|1004|1005|1006|1007|1008|1009|100a|100b|100c|100d|100e|100f|1010|1011|1012|1013|1014|1015|1016|1017|1018|1019|101a|101b|101c|101d|101e|101f", TAG+="uaccess", TAG+="udev-acl"
	SUBSYSTEMS=="usb", ATTRS{idVendor}=="2c97", ATTRS{idProduct}=="0002|2000|2001|2002|2003|2004|2005|2006|2007|2008|2009|200a|200b|200c|200d|200e|200f|2010|2011|2012|2013|2014|2015|2016|2017|2018|2019|201a|201b|201c|201d|201e|201f", TAG+="uaccess", TAG+="udev-acl"
	SUBSYSTEMS=="usb", ATTRS{idVendor}=="2c97", ATTRS{idProduct}=="0003|3000|3001|3002|3003|3004|3005|3006|3007|3008|3009|300a|300b|300c|300d|300e|300f|3010|3011|3012|3013|3014|3015|3016|3017|3018|3019|301a|301b|301c|301d|301e|301f", TAG+="uaccess", TAG+="udev-acl"
	SUBSYSTEMS=="usb", ATTRS{idVendor}=="2c97", ATTRS{idProduct}=="0004|4000|4001|4002|4003|4004|4005|4006|4007|4008|4009|400a|400b|400c|400d|400e|400f|4010|4011|4012|4013|4014|4015|4016|4017|4018|4019|401a|401b|401c|401d|401e|401f", TAG+="uaccess", TAG+="udev-acl"
	EOF
  sudo udevadm trigger
  sudo udevadm control --reload

  binFile="$HOME/bin/ledger"
  rm -f "$binFile"
  wget -O "$binFile" https://download-live.ledger.com/releases/latest/download/linux
  chmod +x "$binFile"
else
  echo "Ledger Live is already installed"
fi

# Pandoc
if [[ -z "$(command -v pandoc)" ]]
then
  echo
  echo "Installing Pandoc"
  sudo apt-get install -y texlive-latex-recommended texlive-fonts-recommended texlive-xetex
  rm -f /tmp/pandoc.deb
  wget -O /tmp/pandoc.deb https://github.com/jgm/pandoc/releases/download/2.11.3.2/pandoc-2.11.3.2-1-amd64.deb
  sudo dpkg -i /tmp/pandoc.deb
else
  echo "Pandoc is already installed"
fi

# Calibre
if [[ -z "$(command -v calibre)" ]]
then
  echo
  echo "Installing Calibre"
  dest="$HOME/Downloads/calibre-installer.sh"
  if [[ ! -f "$dest" ]]
  then wget -O "$dest" https://download.calibre-ebook.com/linux-installer.sh
  fi
  echo "Calibre installer has been downloaded, executing it now.."
  sudo sh "$dest"
  deDRM="$HOME/Downloads/deDRM.zip"
  if [[ ! -f "$deDRM" ]]
  then wget -O "$deDRM" https://github.com/apprenticeharper/DeDRM_tools/releases/download/v7.1.0/DeDRM_tools_7.1.0.zip
  fi
  cd "$(dirname "$deDRM")" || exit 1
  unzip "$deDRM"
  calibre-customize --add DeDRM_plugin.zip
else
  echo "Calibre is already installed"
fi

# VLC
if [[ -z "$(command -v vlc)" ]]
then
  echo
  echo "Installing VLC"
  sudo snap install vlc
else
  echo "VLC is already installed"
fi

# Discord
if [[ -z "$(command -v discord)" ]]
then
  echo
  echo "Installing Discord"
  sudo snap install discord
else
  echo "Discord is already installed"
fi

# Libre Office
if [[ -z "$(command -v libreoffice)" ]]
then
  echo
  echo "Installing Libre Office"
  sudo snap install libreoffice
else
  echo "Libre Office is already installed"
fi

# Gnupg
if [[ -z "$(command -v gpg)" ]]
then
  echo
  echo "Installing Gnu Privacy Guard"
  sudo apt install -y gnupg
else
  echo "Gnu Privacy Guard is already installed"
fi

# Bat
if [[ -z "$(command -v bat)" ]]
then
  echo
  echo "Installing Bat"
  rm -f /tmp/bat.deb
  wget -O /tmp/bat.deb https://github.com/sharkdp/bat/releases/download/v0.17.1/bat_0.17.1_amd64.deb
  sudo dpkg -i /tmp/bat.deb
else
  echo "Bat is already installed"
fi

# Golang
if [[ -z "$(command -v go)" ]]
then
  echo
  echo "Installing Go"
  wget -c https://dl.google.com/go/go1.14.4.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local
else
  echo "Go is already installed"
fi

# Github CLI
if [[ -z "$(command -v gh)" ]]
then
  echo
  echo "Installing Github CLI"
  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key C99B11DEB97541F0
  sudo apt-add-repository -y https://cli.github.com/packages
  sudo apt update -y
  sudo apt install -y gh
else
  echo "Github CLI is already installed"
fi

# OpenVPN
if [[ -z "$(command -v openvpn)" ]]
then
  echo
  echo "Installing OpenVPN"
  sudo apt install -y network-manager-openvpn-gnome openvpn
  # Run below command to import a config file (h/t https://askubuntu.com/a/1071042)
  # sudo nmcli connection import type openvpn file /path/to/foo.ovpn
else
  echo "OpenVPN is already installed"
fi

# Urbit
if [[ -z "$(command -v urbit)" ]]
then
  echo
  echo "Installing Urbit"
  (
    cd /tmp
    rm -rf urbit
    mkdir urbit
    cd urbit
    wget --content-disposition https://urbit.org/install/linux64/latest
    tar zxvf ./linux64.tgz --strip=1
    mv urbit "$HOME/bin/urbit"
  )
else
  echo "Urbit is already installed"
fi

# Maser PDF Editor
if [[ -z "$(command -v masterpdfeditor5)" ]]
then
  echo
  echo "Installing Master PDF Editor"
  sudo apt install -y libqt5printsupport5
  rm -f /tmp/masterpdfeditor5.deb
  wget -O /tmp/masterpdfeditor5.deb https://code-industry.net/public/master-pdf-editor-5.7.20-qt5.x86_64.deb
  sudo dpkg -i /tmp/masterpdfeditor5.deb
else
  echo "Master PDF Editor is already installed"
fi

# Imagemagick
if [[ -z "$(command -v convert-im6.q16)" ]]
then
  echo
  echo "Installing Imagemagick"
  sudo apt install -y imagemagick
else
  echo "Imagemagick is already installed"
fi

# Inkscape
if [[ -z "$(command -v inkscape)" ]]
then
  echo
  echo "Installing Inkscape"
  sudo apt install -y inkscape
else
  echo "Inkscape is already installed"
fi

# qBittorrent
if [[ -z "$(command -v qbittorrent)" ]]
then
  echo
  echo "Installing qBittorrent"
  sudo apt install -y qbittorrent
else
  echo "qBittorrent is already installed"
fi

# HEIF utils
if [[ -z "$(command -v heif-convert)" ]]
then
  echo
  echo "Installing HEIF utils"
  sudo apt-get install -y libheif-examples
else
  echo "HEIF utils are already installed"
fi

# Conversion utils
if [[ -z "$(command -v doc2pdf)" ]]
then
  echo
  echo "Installing conversion utils"
  sudo apt-get install -y unoconv
else
  echo "Conversion utils are already installed"
fi

########################################
# Upgrade everything & auto-remove unnecessary stuff

echo
echo "Checking for packages to upgrade or remove"
sudo apt-get upgrade -y
sudo apt-get autoremove -y
echo "Finished installing & upgrading"
echo

########################################
# Setup Symlinks

echo "Setting up symlinks"

all=${ETC_DIR:-$HOME/all}
etc=${ETC_DIR:-$all/Documents/etc}
media=${MEDIA_DIR:-$all/Documents/media}

if [[ ! -d "$all" ]]
then
  echo "Creating ~/all & moving important stuff into it"
  mkdir -pv "$all"  
  mv -v "$HOME/Documents" "$all/Documents"
  ln -sfTv "$all/Documents" "$HOME/Documents"
  mv -v "$HOME/Dropbox" "$all/Dropbox"
  ln -sfTv "$all/Dropbox" "$HOME/Dropbox"
fi

if [[ -d "$HOME/Media" && ! -L "$HOME/Media" ]]
then
  mv -v "$HOME/Media" "$all/Media"
  if [[ ! -d "$HOME/Media" && ! -L "$HOME/Media" ]]
  then ln -sfTv "$all/Media" "$HOME/Media"
  fi
else
  mkdir -pv "$HOME/Media"
fi

if [[ ! -d "$etc" ]]
then
  echo "$etc doesn't exist, skipping dotfile link creation"
else
  # link dot files
  ln -sfTv "$etc/bash_aliases" "$HOME/.bash_aliases"
  ln -sfTv "$etc/bash_logout" "$HOME/.bash_logout"
  ln -sfTv "$etc/bashrc" "$HOME/.bashrc"
  ln -sfTv "$etc/desktop_aliases" "$HOME/.desktop_aliases"
  ln -sfTv "$etc/eslintrc.json"  "$HOME/.eslintrc.json"
  ln -sfTv "$etc/gitconfig"  "$HOME/.gitconfig"
  ln -sfTv "$etc/inputrc" "$HOME/.inputrc"
  ln -sfTv "$etc/profile" "$HOME/.profile"
  ln -sfTv "$etc/pylintrc"  "$HOME/.pylintrc"
  ln -sfTv "$etc/vimrc"  "$HOME/.vimrc"
  ln -sfTv "$etc/vimrc"  "$HOME/.config/nvim/init.vim"
fi

# Link common shortcuts to home dir
ln -sfTv "$all/Documents/blog-content" "$HOME/b"
ln -sfTv "$all/Documents" "$HOME/d"
ln -sfTv "$all/Documents/etc" "$HOME/etc"
ln -sfTv "$all/Documents/go" "$HOME/go"
ln -sfTv "$all/Dropbox/journal" "$all/Documents/journal"
ln -sfTv "$all/Documents/journal" "$HOME/j"
ln -sfTv "$all/Media" "$HOME/m"
ln -sfTv "$all/Documents/notes" "$HOME/n"
ln -sfTv "$all/Dropbox/todo" "$HOME/.todo"
ln -sfTv "$HOME/.todo" "$HOME/t"
ln -sfTv "$all/Dropbox/Shared" "$HOME/s"

# Add utils to our home's bin
for util in "$etc/bin/"* "$media/bin/"*
do ln -sfTv "$util" "$HOME/bin/$(basename "$util")"
done

ln -sfTv "$HOME/Documents/journal/rjournal" "$HOME/bin/rjournal"
ln -sfTv "$HOME/.todo/todo.sh" "$HOME/bin/todo"
